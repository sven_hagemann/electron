const electron = require("electron");
const ipc = electron.ipcRenderer;

const asyncBtn =  document.getElementById('asyncBtn');
const syncBtn =  document.getElementById('syncBtn');

// Send Event "open-error-dialog" to Server-Side
asyncBtn.addEventListener('click', function () {
    console.log('async msg 1');   
    ipc.send('async-message');
    console.log('async msg 2');   
});

// Send Event "open-error-dialog" to Server-Side
syncBtn.addEventListener('click', function () {
    console.log('sync msg 1');   
    const reply = ipc.sendSync('sync-message');
    console.log(reply);   
    console.log('sync msg 2');   
});

// Get event from server
ipc.on('async-reply', function (event, arg) {
    console.log(arg);    
});


const BrowserWindow = electron.remote.BrowserWindow;
let myWindow = new BrowserWindow();
myWindow.loadURL('http://github.com');