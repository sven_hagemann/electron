const fs = require('fs');
const path = require('path');

btnCreate = document.getElementById('btnCreate');
btnRead = document.getElementById('btnRead');
btnDelete = document.getElementById('btnDelete');
fileName = document.getElementById('fileName');
fileContents = document.getElementById('fileContents');

let pathName = path.join(__dirname, 'files');

btnCreate.addEventListener('click', function () {
    let fullFileName = path.join(pathName, fileName.value);
    let contents = fileContents.value;

    fs.writeFile(fullFileName, contents, function (err) {
        if (err) {
            return console.log(err);            
        }
        console.log("The file was created.");
    })
});

btnRead.addEventListener('click', function () {
    let fullFileName = path.join(pathName, fileName.value);
    fs.readFile(fullFileName, function (err, data) {
        if (err) {
            return console.log(err);
        }

        fileContents.value = data;
        console.log('The file was read');
        
    });
});

btnDelete.addEventListener('click', function () {
    let fullFileName = path.join(pathName, fileName.value);
    fs.unlink(fullFileName, function (err) {
        if (err) {
            console.log(err);            
        }

        fileName.value = '';
        fileContents.value = '';
        console.log("The file was deleted!");
        
    })
})