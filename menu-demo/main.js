const electron = require("electron");
const { app, Menu } = electron;
const BrowserWindow = electron.BrowserWindow;
const path = require("path");
const url = require("url");
const MenuItem = electron.MenuItem;

let win;

function createWindow() {
    win = new BrowserWindow({ width: 800, height: 600, webPreferences: { nodeIntegration: true } });

    win.loadURL(url.format({
        pathname: path.join(__dirname, 'index.html'),
        protocol: 'file',
        slashes: true
    }));

    win.webContents.openDevTools();

    win.on('closed', () => {
        win = null;
    });
}

app.on('ready', function () {
    createWindow();

    const template = [
        {
            label: 'Edit',
            submenu: [
                { role: 'undo' },
                { role: 'redo' },
                { type: 'separator' },
                { role: 'cut' },
                { role: 'copy' },
                { role: 'paste' },
                { role: 'delete' },
                { type: 'separator' },
                { role: 'selectall' }   // in electron v. 5.0.1 not working
            ]
        },
        {
            label: 'demo',
            submenu: [
                {
                    label: 'submenu1',
                    click: function () {
                        console.log('Clicked submenu1');
                    }
                },
                {
                    type: 'separator'
                },
                {
                    label: 'submenu2'
                }
            ]
        },
        {
            label: 'Help',
            submenu:[
                {
                    label: 'About electron',
                    click: function () {
                        electron.shell.openExternal('http://electron.atom.io');
                    },
                    accelerator: 'CmdOrCtrl + Shift + H'
                }
            ]
        }
    ]

    const menu = Menu.buildFromTemplate(template);
    Menu.setApplicationMenu(menu);

    const ctxMenu = new Menu();
    ctxMenu.append(new MenuItem({
        label: 'Hello',
        click: function () {
            console.log('Context menu item clicked');            
        }
    }))

    ctxMenu.append(new MenuItem({role: 'selectall'}))

    win.webContents.on('context-menu', function (e, params) {
        ctxMenu.popup(win, params.x, params.y)
    })
});

// app.on('activate', () => {
//     if (win === null) {
//         createWindow();
//     }
// });